#include <stdio.h>
#include <mpfr.h>

#include <arb.h>

#define PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679
#define PREC 256


///////////////// Utils
double compute_elapsed_time(struct timespec start, struct timespec stop){
        long elapsed_seconds, elapsed_nanoseconds ;
        double  elapsed ;

        elapsed_seconds = stop.tv_sec - start.tv_sec;
        elapsed_nanoseconds = stop.tv_nsec - start.tv_nsec;

        elapsed = (double)elapsed_seconds + ((double)elapsed_nanoseconds/1000000000.0) ;
        return elapsed ;
}




////////////////// MPFR functions 
mpfr_t* alloc_array_mpfr(int size){
	mpfr_t* array = (mpfr_t*)malloc(size * sizeof(mpfr_t) ) ;
	return array;
}

void init_array_mpfr(mpfr_t* array, int size, int prec){
	//init with pi 
	//
	gmp_randstate_t state ;
        gmp_randinit_default(state);	
	//
	for (int i = 0 ; i < size; i++){
		mpfr_init2(array[i], prec) ; 
                mpfr_set_d(array[i], 0, MPFR_RNDN) ;
		mpfr_urandomb(array[i], state);
	}
}

void free_array_mpfr(mpfr_t* array, int size){
	for (int i = 0 ; i < size ; i++){
		mpfr_clear(array[i]) ; 
	}
	free(array) ; 
}

void sum_array_mpfr(mpfr_t* array1, mpfr_t* array2, mpfr_t* array3, int size){
	for (int i = 0 ; i < size; i++){
		 mpfr_add(array3[i], array2[i], array1[i], MPFR_RNDN) ;
	}
}


double loop_mpfr(int rep, int size, int prec){
	mpfr_t* array_mpfr1 = alloc_array_mpfr(size) ;
        init_array_mpfr(array_mpfr1, size, prec) ;

        mpfr_t* array_mpfr2 = alloc_array_mpfr(size) ;
        init_array_mpfr(array_mpfr2, size, prec) ;

        mpfr_t* array_mpfr3 = alloc_array_mpfr(size) ;
        init_array_mpfr(array_mpfr3, size, prec) ;

	struct timespec start, stop ; 
	clock_gettime(CLOCK_MONOTONIC, &start);
	for (int i = 0 ; i < rep ; i++){
	       sum_array_mpfr(array_mpfr1, array_mpfr2, array_mpfr3, size);
	}
        clock_gettime(CLOCK_MONOTONIC, &stop);
	double elapsed = compute_elapsed_time(start, stop); 
		elapsed = elapsed / ((double)rep) ;

//	printf("Elapsed time: %.10f seconds\n", elapsed);

        mpfr_printf("%.60Rf\n", array_mpfr3[3]) ;

	free_array_mpfr(array_mpfr1, size);
        free_array_mpfr(array_mpfr2, size) ;
        free_array_mpfr(array_mpfr3, size) ;

	return elapsed ; 
}

void meta_loop_mpfr(int meta_rep, int inner_rep, int size, int prec){
	double elapsed = 0.0; 

	for (int i = 0 ; i < meta_rep; i++){
		elapsed += loop_mpfr(inner_rep, size, prec);
		printf("%d\n", i);
	}
	elapsed = elapsed / (double)meta_rep ; 
        printf("Elapsed time (MPFR) : %.10f seconds\n", elapsed);
}


/////////////// Arb functions 
arf_t* alloc_array_arf(int size){
        arf_t* array = (arf_t*)malloc(size * sizeof(arf_t) ) ;
        return array;
}


void init_array_arf(arf_t* array, int size, int prec){
        //init with pi 
	flint_rand_t state; 
	flint_randinit(state);
	//
        for (int i = 0 ; i < size; i++){
                arf_init(array[i]) ;
                arf_set_d(array[i], 0) ;
		arf_urandom(array[i], state, prec, ARF_RND_DOWN) ;
	}
}

void free_array_arf(arf_t* array, int size){
        for (int i = 0 ; i < size ; i++){
                arf_clear(array[i]) ;
        }
        free(array) ;
}

void sum_array_arf(arf_t* array1, arf_t* array2, arf_t* array3, int size, int prec){
        for (int i = 0 ; i < size; i++){
                 arf_add(array3[i], array2[i], array1[i], prec, ARF_RND_DOWN) ;
        }
}


double loop_arf(int rep, int size, int prec){
        arf_t* array_arf1 = alloc_array_arf(size) ;
        init_array_arf(array_arf1, size, prec) ;

        arf_t* array_arf2 = alloc_array_arf(size) ;
        init_array_arf(array_arf2, size, prec) ;

        arf_t* array_arf3 = alloc_array_arf(size) ;
        init_array_arf(array_arf3, size, prec) ;

        struct timespec start, stop ;
        clock_gettime(CLOCK_MONOTONIC, &start);
        for (int i = 0 ; i < rep ; i++){
               sum_array_arf(array_arf1, array_arf2, array_arf3, size, prec);
	}
        clock_gettime(CLOCK_MONOTONIC, &stop);
        double elapsed = compute_elapsed_time(start, stop);
        elapsed = elapsed / ((double)rep) ;

//        printf("Elapsed time: %.10f seconds\n", elapsed);


        arf_printd(array_arf3[3], 60);printf("\n");


        free_array_arf(array_arf1, size);
        free_array_arf(array_arf2, size) ;
        free_array_arf(array_arf3, size) ;

        return elapsed ;
}

void meta_loop_arf(int meta_rep, int inner_rep, int size, int prec){
        double elapsed = 0.0;

        for (int i = 0 ; i < meta_rep; i++){
                elapsed += loop_arf(inner_rep, size, prec);
                printf("%d\n", i);
        }
        elapsed = elapsed / (double)meta_rep ;
        printf("Elapsed time (ARF) : %.10f seconds\n", elapsed);
}





////////////// Main 
int main() {
	int meta_rep = 11 ; 
	int inner_rep= 4;
	int size = 1000000  ;
	int prec = 12800 ; 
	meta_loop_mpfr(meta_rep, inner_rep,  size, prec);

	meta_loop_arf(meta_rep, inner_rep, size, prec);

	return 0;
}

