cmake_minimum_required(VERSION 3.0)
project(proj)


#find_package(MPFR REQUIRED)

set(MPFR_DIR "/home/sbstndbs/arbitrary-precision/external/mpfr")
set(ARB_DIR "/home/sbstndbs/arbitrary-precision/external/arb")
set(GMP_DIR "/home/sbstndbs/arbitrary-precision/external/gmp-6.3.0")


include_directories(${MPFR_DIR}/include)
link_directories(${MPFR_DIR}/lib)

include_directories(${ARB_DIR}/include)
link_directories(${ARB_DIR}/build)

include_directories(${GMP_DIR}/include)
link_directories(${GMP_DIR}/.libs)


# Déclaration de l'exécutable
add_executable(exe src/main.c)

target_link_libraries(exe mpfr arb gmp)
